{-# LANGUAGE FlexibleContexts #-}
-- {{{ Imports
import           XMonad
import           XMonad.Actions.DwmPromote
import           XMonad.Actions.RotSlaves
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.InsertPosition
import           XMonad.Hooks.StatusBar
import           XMonad.Hooks.StatusBar.PP
import           XMonad.Layout.Spacing
import           XMonad.Layout.Dishes
import           XMonad.Layout.Renamed
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.MultiToggle.Instances
import           XMonad.Layout.Master (mastered)
import           XMonad.Layout.Tabbed
import           XMonad.Layout.NoBorders
import qualified XMonad.Layout.Magnifier as M
import           XMonad.Layout.ResizableTile
import           XMonad.Layout.StackTile
import           XMonad.Prompt
import           XMonad.Prompt.RunOrRaise
import           XMonad.Util.SpawnOnce
import           XMonad.Util.EZConfig (mkKeymap)
import           Data.Tree
import           Data.Monoid
import           System.Exit

import qualified Data.Map        as Map
import qualified XMonad.StackSet as W
-- }}}
-- {{{ Configs
xpC = def
  { font = "xft:JetBrainsMono Nerd Font"
  , bgColor = "#222222"
  , fgColor = "grey"
  , promptBorderWidth = 0
  , height = 18
  , showCompletionOnTab = False
}

ppC = def
  { ppCurrent = wrap "[" "]" . xmobarColor "grey" "#222222"
  , ppHidden = xmobarColor "grey" "#222222"
  , ppSep = " | "
}

tabC = def
    { activeColor   = "#222222"
    , inactiveColor = "#222222"
    , urgentColor   = "#222222"
    , inactiveBorderColor = "#222222"
    , activeBorderColor = "#222222"
    , urgentBorderColor = "#FF0000"
    , fontName = "xft:JetBrainsMono Nerd Font:size=9"
    , activeTextColor = "#005577"
    , inactiveTextColor = "grey"
    , urgentTextColor = "#FF0000"
    , decoWidth = 180
}
-- }}}
-- {{{ Keys
keys' = [ ("M-S-<Return>", spawn "kitty")
        , ("M-<Space>", runOrRaisePrompt xpC)
        , ("M-t", withFocused $ windows . W.sink)
        , ("M-S-<Space>", sendMessage NextLayout)
        , ("M-,", sendMessage (IncMasterN 1))
        , ("M-.", sendMessage (IncMasterN (-1)))
        , ("M-p", spawn "flameshot gui")
        , ("M-w", kill)
        , ("M-q", spawn "xmonad --recompile; xmonad --restart")
        , ("M-S-q", io exitSuccess)
        , ("<XF86AudioRaiseVolume>", spawn "pulsemixer --change-volume +5")
        , ("<XF86AudioLowerVolume>", spawn "pulsemixer --change-volume -5")
        , ("<XF86AudioMute>", spawn "pulsemixer --toggle-mute")
        , ("M-n", refresh)
        , ("M-h", sendMessage Shrink)
        , ("M-j", windows W.focusDown)
        , ("M-k", windows W.focusUp)
        , ("M-l", sendMessage Expand)
        , ("M-S-h", sendMessage MirrorExpand)
        , ("M-S-j", windows W.swapDown)
        , ("M-S-k", windows W.swapUp)
        , ("M-S-l", sendMessage MirrorShrink)
        , ("M-C-j", rotSlavesDown)
        , ("M-C-k", rotSlavesUp)
        , ("M-<Return> s", dwmpromote)
        , ("M-<Return> f", windows W.focusMaster)
        , ("M-m", sendMessage M.Toggle)
        , ("M-f", sendMessage $ Toggle NBFULL)] ++
        [("M-" ++ show k, windows $ W.view i) | (k, i) <- zip ([1..9] ++ [0]) (map show [1..10])] ++
        [("M-S-" ++ show k, windows $ W.shift i) | (k, i) <- zip ([1..9] ++ [0]) (map show [1..10])]
-- }}}
-- {{{ Layout
-- {{{ Helper functions
rename   x    = renamed [Replace x]
baseMod  x l  = rename x
              $ noBorders 
              $ avoidStruts
              $ M.magnify 1.0 1.5 (M.NoMaster 1) True 
              $ l
baseMod' x l  = rename x
              $ avoidStruts
              $ l
-- }}}
layout = mkToggle (single NBFULL) (rtile ||| tabs) where
      rtile = baseMod  "rtile" (ResizableTall 1 (3/100) (1/2) [])
      tabs  = baseMod' "tabs"  tabs'
      tabs' = noBorders $ (mastered (3/100) (1/2) $ tabbed shrinkText tabC)
-- }}}
-- {{{ Main
main = xmonad . withSB (statusBarProp "xmobar" $ pure ppC) . docks $ def
  { modMask = mod4Mask
  , keys = \c -> mkKeymap c keys'
  , terminal = "kitty"
  , workspaces = map show [1..10]
  , layoutHook = layout
  , manageHook = insertPosition End Newer
  , startupHook = do
      spawnOnce "picom --experimental-backend"
      spawnOnce "flameshot"
      spawnOnce "setxkbmap gb -option caps:swapescape -option compose:ralt"
      spawnOnce "feh --bg-fill /home/lyiriyah/docs/w.png"
  , normalBorderColor = "#333333"
  , focusedBorderColor = "#005577"
  , borderWidth = 1
  }
-- }}}
